export type DishBean = {
    id: number;
    title: string;
    description: string;
    price: number;
    photoURL: string;
};
