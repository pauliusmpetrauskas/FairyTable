export type ChatBean = {
    custumerId: string;
    content: string;
    created_at: Date;
    id: number;
};
